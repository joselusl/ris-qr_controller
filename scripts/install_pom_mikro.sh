#!/bin/bash

# this script install all the packages required by pom and mikrokopter
GREEN='\033[0;32m'

ROBOTPKG_SRC='/home/jlsanche/workspace/robotpkg'

# matlab-genomix
cd $ROBOTPKG_SRC/supervision/matlab-genomix
make update  
echo -e "${GREEN} MATLAB-GENOMIX INSTALLED\n"
sleep 1

# tcl-genomix
cd $ROBOTPKG_SRC/supervision/tcl-genomix
make update   
echo -e "${GREEN} TCL-GENOMIX INSTALLED\n"
sleep 1

# mikrokopter-genom3
cd $ROBOTPKG_SRC/robots/mikrokopter-genom3
make update   
echo -e "${GREEN} MIKROKOPTER-GENOM3 INSTALLED\n"
sleep 1

# optitrack-genom3
cd $ROBOTPKG_SRC/localization/optitrack-genom3
make update   
echo -e "${GREEN} OPTITRACK-GENOM3 INSTALLED\n"
sleep 1

# pom-genom3
cd $ROBOTPKG_SRC/localization/pom-genom3
make update   
echo -e "${GREEN} POM-GENOM3 INSTALLED\n"
sleep 1

# genomix
cd $ROBOTPKG_SRC/net/genomix
make update   
echo -e "${GREEN} GENOMIX INSTALLED\n"
sleep 1

# joystick
cd $ROBOTPKG_SRC/hardware/joystick
make update   
echo -e "${GREEN} JOYSTICK INSTALLED\n"
sleep 1

