from morse.builder import *

cat = Quadrotor()
cat.translate(z = 0)
cat.set_no_collision()

teleport = Teleport()
cat.append(teleport)
teleport.add_stream('socket')

env = Environment('./../blender/adream-ground-with-ap.blend')
env.set_camera_clip(clip_end=1000)
env.set_animation_record()

