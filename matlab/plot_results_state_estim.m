% Script to plot results

%%
close all;


%% State estimator enabled
figure
time_ref=time(1);
plot(time-time_ref,state_estimator_selection);
grid on
title('kind of state estimator used as feedback for the controller');


%% Position Robot wrt World
figure

subplot(3,1,1)
% MSF
plot(time, pom2_p_R_W(:,1),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,1),'b');
grid on
title('position x robot wrt world');

subplot(3,1,2)
% MSF
plot(time, pom2_p_R_W(:,2),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,2),'b');
grid on
title('position y robot wrt world');

subplot(3,1,3)
% MSF
plot(time, pom2_p_R_W(:,3),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,3),'b');
grid on
title('position z robot wrt world');


%% Attitude Robot wrt World
figure

subplot(4,1,1)
% MSF
plot(time, pom2_p_R_W(:,4),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,4),'b');
grid on
title('attitude qw robot wrt world');

subplot(4,1,2)
% MSF
plot(time, pom2_p_R_W(:,5),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,5),'b');
grid on
title('attitude qx robot wrt world');

subplot(4,1,3)
% MSF
plot(time, pom2_p_R_W(:,6),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,6),'b');
grid on
title('attitude qy robot wrt world');

subplot(4,1,4)
% MSF
plot(time, pom2_p_R_W(:,7),'r');
hold on
% GT
plot(time, pom1_p_R_W(:,7),'b');
grid on
title('attitude qz robot wrt world');


%% Linear Velocity Robot wrt World
figure

subplot(3,1,1)
% MSF
plot(time, pom2_v_R_W(:,1),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,1),'b');
grid on
title('linear velocity x robot wrt world');

subplot(3,1,2)
% MSF
plot(time, pom2_v_R_W(:,2),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,2),'b');
grid on
title('linear velocity y robot wrt world');

subplot(3,1,3)
% MSF
plot(time, pom2_v_R_W(:,3),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,3),'b');
grid on
title('linear velocity z robot wrt world');


%% Angular Velocity Robot wrt World
figure

subplot(3,1,1)
% MSF
plot(time, pom2_v_R_W(:,4),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,4),'b');
grid on
title('angular velocity x robot wrt world');

subplot(3,1,2)
% MSF
plot(time, pom2_v_R_W(:,5),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,5),'b');
grid on
title('angular velocity y robot wrt world');

subplot(3,1,3)
% MSF
plot(time, pom2_v_R_W(:,6),'r');
hold on
% GT
plot(time, pom1_v_R_W(:,6),'b');
grid on
title('angular velocity z robot wrt world');




%% Linear Acceleration Robot wrt World
figure

subplot(3,1,1)
% MSF
plot(time, pom2_a_R_W(:,1),'r');
hold on
% GT
plot(time, pom1_a_R_W(:,1),'b');
grid on
title('linear acceleration x robot wrt world');

subplot(3,1,2)
% MSF
plot(time, pom2_a_R_W(:,2),'r');
hold on
% GT
plot(time, pom1_a_R_W(:,2),'b');
grid on
title('linear acceleration y robot wrt world');

subplot(3,1,3)
% MSF
plot(time, pom2_a_R_W(:,3),'r');
hold on
% GT
plot(time, pom1_a_R_W(:,3),'b');
grid on
title('linear acceleration z robot wrt world');


