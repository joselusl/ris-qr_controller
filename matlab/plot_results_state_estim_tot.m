% Script to plot results

%%
close all;


% First interpolate
% Pose
if(~exist('estim1_pose_robot_wrt_world','var'))
    estim1_pose_robot_wrt_world.time=time;
    estim1_pose_robot_wrt_world.position=interpolate_signal(estim_pose_robot_wrt_world.position',estim_pose_robot_wrt_world.time-time(1), time-time(1));
    estim1_pose_robot_wrt_world.position=estim1_pose_robot_wrt_world.position';
    estim1_pose_robot_wrt_world.attitude=interpolate_signal(estim_pose_robot_wrt_world.attitude',estim_pose_robot_wrt_world.time-time(1), time-time(1));
    estim1_pose_robot_wrt_world.attitude=estim1_pose_robot_wrt_world.attitude';
end
% Vel
if(~exist('estim1_vel_robot_wrt_world','var'))
    estim1_vel_robot_wrt_world.time=time;
    estim1_vel_robot_wrt_world.linear=interpolate_signal(estim_vel_robot_wrt_world.linear',estim_vel_robot_wrt_world.time-time(1), time-time(1));
    estim1_vel_robot_wrt_world.linear=estim1_vel_robot_wrt_world.linear';
    estim1_vel_robot_wrt_world.angular=interpolate_signal(estim_vel_robot_wrt_world.angular',estim_vel_robot_wrt_world.time-time(1), time-time(1));
    estim1_vel_robot_wrt_world.angular=estim1_vel_robot_wrt_world.angular';
end
% Acc
if(~exist('estim1_acc_robot_wrt_world','var'))
    estim1_acc_robot_wrt_world.time=time;
    estim1_acc_robot_wrt_world.linear=interpolate_signal(estim_acc_robot_wrt_world.linear',estim_acc_robot_wrt_world.time-time(1), time-time(1));
    estim1_acc_robot_wrt_world.linear=estim1_acc_robot_wrt_world.linear';
    estim1_acc_robot_wrt_world.angular=interpolate_signal(estim_acc_robot_wrt_world.angular',estim_acc_robot_wrt_world.time-time(1), time-time(1));
    estim1_acc_robot_wrt_world.angular=estim1_acc_robot_wrt_world.angular';
end

% Meas mocap
if(~exist('meas1_pose_mocap_wrt_mocap_world','var'))
    meas1_pose_mocap_wrt_mocap_world.time=time;
    meas1_pose_mocap_wrt_mocap_world.position=interpolate_signal(meas_pose_mocap_wrt_mocap_world.position',meas_pose_mocap_wrt_mocap_world.time-time(1), time-time(1));
    meas1_pose_mocap_wrt_mocap_world.position=meas1_pose_mocap_wrt_mocap_world.position';
    meas1_pose_mocap_wrt_mocap_world.attitude=interpolate_signal(meas_pose_mocap_wrt_mocap_world.attitude',meas_pose_mocap_wrt_mocap_world.time-time(1), time-time(1));
    meas1_pose_mocap_wrt_mocap_world.attitude=meas1_pose_mocap_wrt_mocap_world.attitude';
end


% Process norms
num_samples=size(pom1_p_R_W,1);
% Norm position
n_posit_1=zeros(num_samples, 1);
for(i=1:num_samples)
    n_posit_1(i,1)=norm(pom1_p_R_W(i,1:3)-pom2_p_R_W(i,1:3));
end
n_posit_2=zeros(num_samples, 1);
for(i=1:num_samples)
    n_posit_2(i,1)=norm(estim1_pose_robot_wrt_world.position(1:3,i)'-pom2_p_R_W(i,1:3));
end
n_posit_3=zeros(num_samples, 1);
for(i=1:num_samples)
    n_posit_3(i,1)=norm(estim1_pose_robot_wrt_world.position(1:3,i)'-pom1_p_R_W(i,1:3));
end
% Norm velocity
n_lin_vel_1=zeros(num_samples, 1);
for(i=1:num_samples)
    n_lin_vel_1(i,1)=norm(pom1_v_R_W(i,1:3)-pom2_v_R_W(i,1:3));
end
n_lin_vel_2=zeros(num_samples, 1);
for(i=1:num_samples)
    n_lin_vel_2(i,1)=norm(estim1_vel_robot_wrt_world.linear(1:3,i)'-pom2_v_R_W(i,1:3));
end
n_lin_vel_3=zeros(num_samples, 1);
for(i=1:num_samples)
    n_lin_vel_3(i,1)=norm(estim1_vel_robot_wrt_world.linear(1:3,i)'-pom1_v_R_W(i,1:3));
end



% MAP of landmarks
if(exist('tf','var'))
    
    num_samp_tf=size(tf.time,2);

    parent_world=strfind(tf.parent,'world');

    % VM 3
    if(~exist('p_vm3_w','var'))
        child_vm3=strfind(tf.children,'visual_marker_3');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm3{i}))
                p_vm3_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm3_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm3_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    % VM 4
    if(~exist('p_vm4_w','var'))
        child_vm4=strfind(tf.children,'visual_marker_4');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm4{i}))
                p_vm4_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm4_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm4_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    % VM 11
    if(~exist('p_vm11_w','var'))
        child_vm11=strfind(tf.children,'visual_marker_11');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm11{i}))
                p_vm11_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm11_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm11_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    % VM 15
    if(~exist('p_vm15_w','var'))
        child_vm15=strfind(tf.children,'visual_marker_15');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm15{i}))
                p_vm15_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm15_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm15_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    % VM 16
    if(~exist('p_vm16_w','var'))
        child_vm16=strfind(tf.children,'visual_marker_16');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm16{i}))
                p_vm16_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm16_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm16_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    % VM 18
    if(~exist('p_vm18_w','var'))
        child_vm18=strfind(tf.children,'visual_marker_18');
        num_frame_i=1;
        for(i=1:num_samp_tf)
            if(~isempty(parent_world{i}) && ~isempty(child_vm18{i}))
                p_vm18_w.time(1,num_frame_i)=tf.time(1,i);
                p_vm18_w.position(:,num_frame_i)=tf.position(:,i);
                p_vm18_w.attitude(:,num_frame_i)=tf.attitude(:,i);
                num_frame_i=num_frame_i+1;
            end
        end
    end

    
else
    
    % Create empty structs
    
    % VM 3
    p_vm3_w.time=[];
    p_vm3_w.position=zeros(3,0);
    p_vm3_w.attitude=zeros(4,0);
    
    % VM 4
    p_vm4_w.time=[];
    p_vm4_w.position=zeros(3,0);
    p_vm4_w.attitude=zeros(4,0);
    
    % VM 11
    p_vm11_w.time=[];
    p_vm11_w.position=zeros(3,0);
    p_vm11_w.attitude=zeros(4,0);
    
    % VM 15
    p_vm15_w.time=[];
    p_vm15_w.position=zeros(3,0);
    p_vm15_w.attitude=zeros(4,0);
    
    % VM 16
    p_vm16_w.time=[];
    p_vm16_w.position=zeros(3,0);
    p_vm16_w.attitude=zeros(4,0);
    
    % VM 18
    p_vm18_w.time=[];
    p_vm18_w.position=zeros(3,0);
    p_vm18_w.attitude=zeros(4,0);
    
end



% Display configs
config.display.kind_state_estimator_enabled=0;
config.display.measurements.imu=1;


%% State estimator enabled
if(config.display.kind_state_estimator_enabled)
    figure
    time_ref=time(1);
    plot(time-time_ref,state_estimator_selection);
    grid on
    axis tight
    title('kind of state estimator used as feedback for the controller');
end


%% Position Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% Mocap
plot(time-time_ref, meas1_pose_mocap_wrt_mocap_world.position(1,:)','y');
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,1),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,1),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.position(1,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.position(1,:)','c')
grid on
axis tight
title('position x robot wrt world');

subplot(3,1,2)
hold on
% Mocap
plot(time-time_ref, meas1_pose_mocap_wrt_mocap_world.position(2,:)','y');
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,2),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,2),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.position(2,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.position(2,:)','c')
grid on
axis tight
title('position y robot wrt world');

subplot(3,1,3)
hold on
% Mocap
plot(time-time_ref, meas1_pose_mocap_wrt_mocap_world.position(3,:)','y');
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,3),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,3),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.position(3,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.position(3,:)','c')
grid on
axis tight
title('position z robot wrt world');


%% Position Error Robot wrt World

figure
time_ref=time(1);

subplot(3,1,1)
hold on
% POM-MSF vs POM-Mocap
plot(time-time_ref, pom1_p_R_W(:,1)-pom2_p_R_W(:,1),'r');
% MSF vs POM-Mocap
plot(time-time_ref, estim1_pose_robot_wrt_world.position(1,:)'-pom2_p_R_W(:,1),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_pose_robot_wrt_world.position(1,:)'-pom1_p_R_W(:,1),'c');
grid on
axis tight
title('position error x robot wrt world');

subplot(3,1,2)
hold on
% POM-MSF vs POM-Mocap
plot(time-time_ref, pom1_p_R_W(:,2)-pom2_p_R_W(:,2),'r');
% MSF vs POM-Mocap
plot(time-time_ref, estim1_pose_robot_wrt_world.position(2,:)'-pom2_p_R_W(:,2),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_pose_robot_wrt_world.position(2,:)'-pom1_p_R_W(:,2),'c');
grid on
axis tight
title('position error y robot wrt world');

subplot(3,1,3)
hold on
% POM-MSF vs POM-Mocap
plot(time-time_ref, pom1_p_R_W(:,3)-pom2_p_R_W(:,3),'r');
% MSF vs POM-Mocap
plot(time-time_ref, estim1_pose_robot_wrt_world.position(3,:)'-pom2_p_R_W(:,3),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_pose_robot_wrt_world.position(3,:)'-pom1_p_R_W(:,3),'c');
grid on
axis tight
title('position error z robot wrt world');


%% Position error norm

figure
time_ref=time(1);

hold on
% POM-MSF vs POM-Mocap
plot(time-time_ref, n_posit_1,'r');
% MSF vs POM-Mocap
plot(time-time_ref, n_posit_2,'b');
% MSF vs POM-MSF
plot(time-time_ref, n_posit_3,'c');
grid on
title('position error norm robot wrt world');
axis tight
ylim([0,0.2])



%% Trajectory Robot

figure
hold on

% POM-Mocap
plot3(pom2_p_R_W(:,1), pom2_p_R_W(:,2), pom2_p_R_W(:,3),'r')
% POM-MSF
plot3(pom1_p_R_W(:,1), pom1_p_R_W(:,2), pom1_p_R_W(:,3),'b')
% MSF
plot3(estim1_pose_robot_wrt_world.position(1,:)', estim1_pose_robot_wrt_world.position(2,:)', estim1_pose_robot_wrt_world.position(3,:)','c')

grid on
title('Trajectory of the robot')
xlabel('x (m)')
ylabel('y (m)')
zlabel('z (m)')
axis equal



%% Attitude Robot wrt World
figure
time_ref=time(1);

subplot(4,1,1)
hold on
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,4),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,4),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.attitude(1,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.attitude(1,:)','c')
grid on
axis tight
title('attitude qw robot wrt world');

subplot(4,1,2)
hold on
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,5),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,5),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.attitude(2,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.attitude(2,:)','c')
grid on
axis tight
title('attitude qx robot wrt world');

subplot(4,1,3)
hold on
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,6),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,6),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.attitude(3,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.attitude(3,:)','c')
grid on
axis tight
title('attitude qy robot wrt world');

subplot(4,1,4)
hold on
% POM-Mocap
plot(time-time_ref, pom2_p_R_W(:,7),'r');
% POM-MSF
plot(time-time_ref, pom1_p_R_W(:,7),'b');
% MSF
% plot(estim_pose_robot_wrt_world.time-time_ref, estim_pose_robot_wrt_world.attitude(4,:)','c')
plot(estim1_pose_robot_wrt_world.time-time_ref, estim1_pose_robot_wrt_world.attitude(4,:)','c')
grid on
axis tight
title('attitude qz robot wrt world');


% % Attitude Error Robot wrt World
% figure
% time_ref=time(1);
% 
% subplot(4,1,1)
% hold on
% POM-Mocap
% plot(time-time_ref, pom1_p_R_W(:,4)-pom2_p_R_W(:,4),'r');
% grid on
% axis tight
% title('attitude error qw robot wrt world');
% 
% subplot(4,1,2)
% hold on
% POM-Mocap
% plot(time-time_ref, pom1_p_R_W(:,5)-pom2_p_R_W(:,5),'r');
% grid on
% axis tight
% title('attitude error qx robot wrt world');
% 
% subplot(4,1,3)
% hold on
% POM-Mocap
% plot(time-time_ref, pom1_p_R_W(:,6)-pom2_p_R_W(:,6),'r');
% grid on
% axis tight
% title('attitude error qy robot wrt world');
% 
% subplot(4,1,4)
% hold on
% POM-Mocap
% plot(time-time_ref, pom1_p_R_W(:,7)-pom2_p_R_W(:,7),'r');
% grid on
% axis tight
% title('attitude error qz robot wrt world');


%% Linear Velocity Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,1),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,1),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.linear(1,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.linear(1,:)','c')
grid on
axis tight
title('linear velocity x robot wrt world');

subplot(3,1,2)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,2),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,2),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.linear(2,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.linear(2,:)','c')
grid on
axis tight
title('linear velocity y robot wrt world');

subplot(3,1,3)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,3),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,3),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.linear(3,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.linear(3,:)','c')
grid on
axis tight
title('linear velocity z robot wrt world');


%% Linear Velocity Error Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% POM-Mocap
plot(time-time_ref, pom1_v_R_W(:,1)-pom2_v_R_W(:,1),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(1,:)'-pom2_v_R_W(:,1),'b');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(1,:)'-pom1_v_R_W(:,1),'c');
grid on
axis tight
title('linear velocity error x robot wrt world');

subplot(3,1,2)
hold on
% POM-Mocap
plot(time-time_ref, pom1_v_R_W(:,2)-pom2_v_R_W(:,2),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(2,:)'-pom2_v_R_W(:,2),'b');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(2,:)'-pom1_v_R_W(:,2),'c');
grid on
axis tight
title('linear velocity error y robot wrt world');

subplot(3,1,3)
hold on
% POM-Mocap
plot(time-time_ref, pom1_v_R_W(:,3)-pom2_v_R_W(:,3),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(3,:)'-pom2_v_R_W(:,3),'b');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.linear(3,:)'-pom1_v_R_W(:,3),'c');
grid on
axis tight
title('linear velocity error z robot wrt world');


%% Lin vel error norm

figure
time_ref=time(1);

hold on
% POM-MSF vs POM-Mocap
plot(time-time_ref, n_lin_vel_1,'r');
% MSF vs POM-Mocap
plot(time-time_ref, n_lin_vel_2,'b');
% MSF vs POM-MSF
plot(time-time_ref, n_lin_vel_3,'c');
grid on
title('lin vel error norm robot wrt world');
axis tight
%ylim([0,0.2])



%% Angular Velocity Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,4),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,4),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.angular(1,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.angular(1,:)','c')
grid on
axis tight
title('angular velocity x robot wrt world');

subplot(3,1,2)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,5),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,5),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.angular(2,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.angular(2,:)','c')
grid on
axis tight
title('angular velocity y robot wrt world');

subplot(3,1,3)
hold on
% POM-Mocap
plot(time-time_ref, pom2_v_R_W(:,6),'r');
% POM-MSF
plot(time-time_ref, pom1_v_R_W(:,6),'b');
% MSF
% plot(estim_vel_robot_wrt_world.time-time_ref, estim_vel_robot_wrt_world.angular(3,:)','c')
plot(estim1_vel_robot_wrt_world.time-time_ref, estim1_vel_robot_wrt_world.angular(3,:)','c')
grid on
axis tight
title('angular velocity z robot wrt world');



%% Angular Velocity Error Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% POM-Mocap
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(1,:)'-pom1_v_R_W(:,4),'c');
%
plot(time-time_ref, pom1_v_R_W(:,4)-pom2_v_R_W(:,4),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(1,:)'-pom2_v_R_W(:,4),'b');
grid on
axis tight
title('angular velocity error x robot wrt world');

subplot(3,1,2)
hold on
% POM-Mocap
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(2,:)'-pom1_v_R_W(:,5),'c');
%
plot(time-time_ref, pom1_v_R_W(:,5)-pom2_v_R_W(:,5),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(2,:)'-pom2_v_R_W(:,5),'b');
grid on
axis tight
title('angular velocity error y robot wrt world');

subplot(3,1,3)
hold on
% POM-Mocap
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(3,:)'-pom1_v_R_W(:,6),'c');
%
plot(time-time_ref, pom1_v_R_W(:,6)-pom2_v_R_W(:,6),'r');
%
plot(time-time_ref, estim1_vel_robot_wrt_world.angular(3,:)'-pom2_v_R_W(:,6),'b');
grid on
axis tight
title('angular velocity error z robot wrt world');



%% Linear Acceleration Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% MSF
%plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.linear(1,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.linear(1,:)','c')
% POM-Mocap
plot(time-time_ref, pom2_a_R_W(:,1),'r');
% POM-MSF
plot(time-time_ref, pom1_a_R_W(:,1),'b');
grid on
axis tight
title('linear acceleration x robot wrt world');

subplot(3,1,2)
hold on
% MSF
% plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.linear(2,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.linear(2,:)','c')
% POM-Mocap
plot(time-time_ref, pom2_a_R_W(:,2),'r');
% POM-MSF
plot(time-time_ref, pom1_a_R_W(:,2),'b');
grid on
axis tight
title('linear acceleration y robot wrt world');

subplot(3,1,3)
hold on
% MSF
% plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.linear(3,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.linear(3,:)','c')
% POM-Mocap
plot(time-time_ref, pom2_a_R_W(:,3),'r');
% POM-MSF
plot(time-time_ref, pom1_a_R_W(:,3),'b');
grid on
axis tight
title('linear acceleration z robot wrt world');



%% Linear Acceleration Error Robot wrt World
figure
time_ref=time(1);

subplot(3,1,1)
hold on
% MSF vs POM-MOCAP
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(1,:)'-pom2_a_R_W(:,1),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(1,:)'-pom1_a_R_W(:,1),'c');
% POM-MSF vs POM-MOCAP
plot(time-time_ref, pom1_a_R_W(:,1)-pom2_a_R_W(:,1),'r');
grid on
axis tight
title('linear acceleration error x robot wrt world');

subplot(3,1,2)
hold on
% MSF vs POM-MOCAP
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(2,:)'-pom2_a_R_W(:,2),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(2,:)'-pom1_a_R_W(:,2),'c');
% POM-MSF vs POM-MOCAP
plot(time-time_ref, pom1_a_R_W(:,2)-pom2_a_R_W(:,2),'r');
grid on
axis tight
title('linear acceleration error y robot wrt world');

subplot(3,1,3)
hold on
% MSF vs POM-MOCAP
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(3,:)'-pom2_a_R_W(:,3),'b');
% MSF vs POM-MSF
plot(time-time_ref, estim1_acc_robot_wrt_world.linear(3,:)'-pom1_a_R_W(:,3),'c');
% POM-MSF vs POM-MOCAP
plot(time-time_ref, pom1_a_R_W(:,3)-pom2_a_R_W(:,3),'r');
grid on
axis tight
title('linear acceleration error z robot wrt world');




%% Angular Acceleration
figure
time_ref=time(1);

subplot(3,1,1)
hold on;
% plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.angular(1,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.angular(1,:)','c')
title('\alpha_x')
grid on
axis tight

% figure
subplot(3,1,2)
hold on;
% plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.angular(2,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.angular(2,:)','c')
title('\alpha_y')
grid on
axis tight

% figure
subplot(3,1,3)
hold on;
% plot(estim_acc_robot_wrt_world.time-time_ref, estim_acc_robot_wrt_world.angular(3,:)','c')
plot(estim1_acc_robot_wrt_world.time-time_ref, estim1_acc_robot_wrt_world.angular(3,:)','c')
title('\alpha_z')
grid on
axis tight


%% Estimated bias linear acceleration
figure
time_ref=time(1);

% figure
subplot(3,1,1)
hold on;
plot(imu_estimated_bias_lin_acc.time-time_ref, imu_estimated_bias_lin_acc.value(1,:)','c')
title('b_{acc-x}')
grid on
axis tight

% figure
subplot(3,1,2)
hold on;
plot(imu_estimated_bias_lin_acc.time-time_ref, imu_estimated_bias_lin_acc.value(2,:)','c')
title('b_{acc-y}')
grid on
axis tight

% figure
subplot(3,1,3)
hold on;
plot(imu_estimated_bias_lin_acc.time-time_ref, imu_estimated_bias_lin_acc.value(3,:)','c')
title('b_{acc-z}')
grid on
axis tight


%% IMU measurements - accelerometer

if(config.display.measurements.imu)
    
    figure
    time_ref=time(1);

    % figure
    subplot(3,1,1)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.linear_acc(1,:)','r')
    title('z_{acc-x}')
    grid on
    axis tight

    % figure
    subplot(3,1,2)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.linear_acc(2,:)','r')
    title('z_{acc-y}')
    grid on
    axis tight

    % figure
    subplot(3,1,3)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.linear_acc(3,:)','r')
    title('z_{acc-z}')
    grid on
    axis tight

end


%% IMU measurements - gyro

if(config.display.measurements.imu)
    
    figure
    time_ref=time(1);

    % figure
    subplot(3,1,1)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.angular_vel(1,:)','r')
    title('z_{gyr-x}')
    grid on
    axis tight

    % figure
    subplot(3,1,2)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.angular_vel(2,:)','r')
    title('z_{gyr-y}')
    grid on
    axis tight

    % figure
    subplot(3,1,3)
    hold on;
    plot(imu_meas.time-time_ref, imu_meas.angular_vel(3,:)','r')
    title('z_{gyr-z}')
    grid on
    axis tight

end




%% Markers + Trajectory

figure
hold on


% POM-Mocap
plot3(pom2_p_R_W(:,1), pom2_p_R_W(:,2), pom2_p_R_W(:,3),'r')
% POM-MSF
plot3(pom1_p_R_W(:,1), pom1_p_R_W(:,2), pom1_p_R_W(:,3),'b')
% MSF
plot3(estim1_pose_robot_wrt_world.position(1,:)', estim1_pose_robot_wrt_world.position(2,:)', estim1_pose_robot_wrt_world.position(3,:)','c')



% VM 3
if(~isempty(p_vm3_w.position))
    plot3(p_vm3_w.position(1,:), p_vm3_w.position(2,:), p_vm3_w.position(3,:), 'ob');
    text(p_vm3_w.position(1,length(p_vm3_w.position)), p_vm3_w.position(2,length(p_vm3_w.position)), p_vm3_w.position(3,length(p_vm3_w.position)), ...
        ['  VM 3 '],'HorizontalAlignment','left','FontSize',8);
end
% VM 4
if(~isempty(p_vm4_w.position))
    plot3(p_vm4_w.position(1,:), p_vm4_w.position(2,:), p_vm4_w.position(3,:), 'ob');
    text(p_vm4_w.position(1,length(p_vm4_w.position)), p_vm4_w.position(2,length(p_vm4_w.position)), p_vm4_w.position(3,length(p_vm4_w.position)), ...
        ['  VM 4 '],'HorizontalAlignment','left','FontSize',8);
end
% VM 11
if(~isempty(p_vm11_w.position))
    plot3(p_vm11_w.position(1,:), p_vm11_w.position(2,:), p_vm11_w.position(3,:), 'ob');
    text(p_vm11_w.position(1,length(p_vm11_w.position)), p_vm11_w.position(2,length(p_vm11_w.position)), p_vm11_w.position(3,length(p_vm11_w.position)), ...
        ['  VM 11 '],'HorizontalAlignment','left','FontSize',8);
end
% VM 15
if(~isempty(p_vm15_w.position))
    plot3(p_vm15_w.position(1,:), p_vm15_w.position(2,:), p_vm15_w.position(3,:), 'ob');
    text(p_vm15_w.position(1,length(p_vm15_w.position)), p_vm15_w.position(2,length(p_vm15_w.position)), p_vm15_w.position(3,length(p_vm15_w.position)), ...
        ['  VM 15 '],'HorizontalAlignment','left','FontSize',8);
end
% VM 16
if(~isempty(p_vm16_w.position))
    plot3(p_vm16_w.position(1,:), p_vm16_w.position(2,:), p_vm16_w.position(3,:), 'ob');
    text(p_vm16_w.position(1,length(p_vm16_w.position)), p_vm16_w.position(2,length(p_vm16_w.position)), p_vm16_w.position(3,length(p_vm16_w.position)), ...
        ['  VM 16 '],'HorizontalAlignment','left','FontSize',8);
end
% VM 18
if(~isempty(p_vm18_w.position))
    plot3(p_vm18_w.position(1,:), p_vm18_w.position(2,:), p_vm18_w.position(3,:), 'ob');
    text(p_vm18_w.position(1,length(p_vm18_w.position)), p_vm18_w.position(2,length(p_vm18_w.position)), p_vm18_w.position(3,length(p_vm18_w.position)), ...
        ['  VM 18 '],'HorizontalAlignment','left','FontSize',8);
end

grid on
axis equal
xlabel('x (m)')
ylabel('y (m)')
zlabel('z (m)')
