%%%%%%%%%%%%%%%%%%%%% INIT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defines all the parameters needed for the simulations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add genomix path
% JL
openrobots_base_path='/home/jlsanche/devel/opt/openrobots/';
% Marco
% openrobots_base_path='/home/mtognon/openrobots/';

% General
addpath([openrobots_base_path,'lib/matlab/']);
addpath([openrobots_base_path,'lib/matlab/simulink']);
addpath([openrobots_base_path,'lib/matlab/simulink/genomix']);


addpath('./Parameters')
%addpath './Trajectory_tests'

%% general parameters

Tsim = 1/500; % sampling time [s]

flag_simulationReal = 2;    % 1: SIMULATION       2: REAL
quadrotorOn = 1;

g = 9.81;               % gravity (N)

%% quadrotor

% physical parameters

% mR = 0.925;
mR = 1.1123;
%mR = 1;                 % mass (Kg)

JR = 0.015*eye(3);      % inertia (Kg*m^2)

d = 0.23;               % lenght arms

c_f = 6.5e-4; %5.95010e-04;      % maps propeller speed into thrust %5.9800e-04; %6.5e-4;

c_t = 1e-5;

c=c_t/c_f;

G=c_f*[ 1  1  1  1      % to pass from force to motVel square

        0  d  0 -d  

        -d  0  d  0 

       +c -c +c -c ];

motVel_min = 20;

motVel_max = 90;    % Hz

fmin = c_f*motVel_min^2 ;

fmax = c_f*motVel_max^2 ;

% initial conditions

pR0 = [0 0 0]';         % initial position (m);

Rr0 = [1 0 0;

       0 1 0;

       0 0 1];         % initial orientation   

   
% Yaw angle commanded
% yaw_des=-pi/4;
yaw_des=pi/4;

   
   
% State estimator
   init_state_estimator_selected=1;

% Hyerarchical controller

run('param_controller_hierGeomCtrl.m')

%
run('param_wayPointsTrajectory.m')
