classdef StatesDefinition  < Simulink.IntEnumType
    enumeration
        Ground(0)
        StepMo(1)
        Adjust(2)
        Trajec(3)
        StopHo(4)
        Emerge(5)
        EmerSt(6)
        Landin(7)
    end    


end

