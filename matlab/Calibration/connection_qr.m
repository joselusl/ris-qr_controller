%%
close all
clear all
clc

% Adding path
OPENROBOTS_PATH='/home/joselusl/devel/opt/openrobots/';

addpath([OPENROBOTS_PATH,'lib/matlab'])
addpath([OPENROBOTS_PATH,'lib/matlab/simulink'])
addpath([OPENROBOTS_PATH,'lib/matlab/simulink/genomix'])


%%
client = genomix.client();

%% MK
mikro = client.load ( 'mikrokopter');
result = mikro.connect ({ '/ dev / ttyUSB0', ''} , 500000);
string = ['Connecting to mikrokopter : ', result.status ];
disp ( string );

%% POM
pom = client.load ('pom');
result = pom.connect_port ('measure / imu' , 'mikrokopter / imu');
string = [ 'Initializing 1 st connection of POM : ', result.status ];
pom.add_measurement ('imu');
disp ( string );

%% OPITRACK
opti = client.load ( 'optitrack');
result = opti.connect ( 'marey' , '1510' , '239.192.168.30' , '1511');
string = [ 'Connecting to MoCap : ', result.status ];
disp ( string );

%%
result = pom.connect_port ( 'measure/mocap' , 'optitrack/bodies/QR_4');
string = [ 'Initializing 2 nd connection of POM : ', result.status ];
pom.add_measurement ( ’ mocap ’);
disp ( string );
mikro.set_sensor_rate (1000 ,1 ,1) % IMU - motor - battery

