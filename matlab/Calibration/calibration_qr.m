%%
close all
clear all
clc

% Adding path
OPENROBOTS_PATH='/home/joselusl/devel/opt/openrobots/';

addpath([OPENROBOTS_PATH,'lib/matlab'])
addpath([OPENROBOTS_PATH,'lib/matlab/simulink'])
addpath([OPENROBOTS_PATH,'lib/matlab/simulink/genomix'])


%% Client
client = genomix.client();

%% MK
mikro = client.load ( 'mikrokopter');
result = mikro.connect ({ '/dev/ttyUSB0', ''} , 500000);
string = ['Connecting to mikrokopter : ', result.status ];
disp ( string );


%% IMU Calibration
time_imu_calib=2;
num_pos_imu_calib=10;
mikro.calibrate_imu ( time_imu_calib , num_pos_imu_calib );
% move the imu


%% Set Zero in MOCAP
% Place QR in mocap and create the object
pause
mikro.set_zero()


%% Get IMU Calibration parameters
calibration = mikro.get_imu_calibration();

%% Load the saved calibration parameters
mikro.set_imu_calibration( calibration.result );

