function pos = vect_smoother( pos_raw, N, F )
% Using the Savitzky-Golay FIR smoothing filter we smooth the position and
% we retrieve the velocity and acceleration
%
% INPUT:
% pos_raw : matrix whose column are the position trajectories
% dx : time sampling
% N : order of the polynomial
% F : window length

% Data initialization
SG0 = zeros(size(pos_raw));   % initialize pos

n = size(pos_raw,1);
m = size(pos_raw,2);

% Filter
[b,g] = sgolay(N,F);   % Calculate S-G coefficients

% Increase pos_raw data to deal with the filtering window
y = [ones(round(F*1.5),m)*diag(pos_raw(1,:)); pos_raw; ones(round(F*1.5),m)*diag(pos_raw(end,:))]; 

% Filtering
HalfWin  = ((F+1)/2) -1;

for i=1:m
    for j = (F+1)/2:size(y,1) -(F+1)/2,
      % Zeroth derivative (smoothing only)
      SG0(j,i) = dot(g(:,1),y(j - HalfWin:j + HalfWin,i));
    end
end

pos = SG0(1+round(F*1.5):end,:);


end

