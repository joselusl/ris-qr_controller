%% PRINT CONTROL RESULTS: 


%% General variables
% colors
save_ck=1;
b = [0.0000    0.4470    0.7410];
r = [0.8500    0.3250    0.0980];
y = [0.9290    0.6940    0.1250];
g = [0.4660    0.6740    0.1880];

 save = 1;
LineWidth = 1;
FontSize = 18;
FontSize_label = 14;
FontSize_axes = 12;
Position = [800 60];
Dimension = [800 700];

perc_limits = 0.05;
dy = 0.2;

r2g = 180/pi;
g2r = pi/180;

dim_raw = 2;
dim_col = 2;

ylabh1 = zeros(dim_raw,1);
ylabh2 = zeros(dim_raw,1);
ylabPos1 = zeros(3,dim_raw);
ylabPos2 = zeros(3,dim_raw);
ylabh1_index = 1;
ylabh2_index = 1;

%% Increase the max so the plot is under the legend
alpha_max=0.85;
%% Plot
% fig = figure('name','Outs','Position',[2100 500 700 700]);    % external
% monitor
fig = figure('name','Control results','Position',[Position, Dimension]);
set(fig,'defaulttextinterpreter','latex');
set(fig,'DefaultTextFontname', 'CMU Time');

%% Definition of the angular position
%eta=[rp_hat_imu yaw_NH];
%% Time in the right moment of the experiment


switch_time = nonzeros(state_time);
t = stime;
% t_init = t(1); % ---> Define the initial time
t_init = switch_time(1);
t_end = t(end);
x_limits = [t_init t_end];

%% Freqency for plotting
tc_plot = 0.1;
dt = 0.01; 
%%
index_step = tc_plot/dt;
index_start = find(stime >= x_limits(1),1);
index_end = find(stime>= x_limits(2),1);
windows_plot=index_start:index_step:index_end;
index_start = find(stime>= x_limits(1),1);
index_end = find(stime>= x_limits(2),1);
windows_plot_var=index_start:index_step:index_end;
time_window=windows_plot;
time_window_var=windows_plot_var;
t_new=stime(time_window);


ha = tight_subplot(dim_raw,dim_col,[.01 .09],[.07 .01],[.081 .017]); % (dim_h, dim_w, mar_axes[h,w], mar_hBox[b,t], mar_wBox[l,r])
%t=t_init:tc_plot:t_end;
%t_new = t- t(1);
%t_new=t_new(windows_plot_THvar);
%t_new=stime(time_window);
%t_new=stime(time_window)-t_init;
%x_limits = [0 t_end-t_init];

i = 1;
%% Forces on every propeller
% for k=1:size(stime,1)
%     vel=motor_speed_des(k,:);
%     vel=[vel(1)^2;vel(2)^2;vel(3)^2;vel(4)^2];
%     prop_forces(k,:)=c_f*vel;
% end

%% Filtering Data
%% Forces
% prop_filt=vect_smoother(prop_forces,2,121);
% thrust_opacity=0.15;
%% Omega
% omegaR_filt=vect_smoother(omegaR,3,91);
% omega_opacity=0.15;

%% Roll & Pitch
    axes(ha(i))
    set(gca,'FontSize',FontSize_axes)    
        plot(t_new, RPY(windows_plot,1)*r2g, 'Color', r, 'LineWidth', LineWidth );
        hold on;
        plot(t_new, RPY(windows_plot,2)*r2g, 'Color', g, 'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)
        
        plot(t_new, RPY_d(windows_plot,1)*r2g, 'Color', r, 'LineWidth', LineWidth, 'LineStyle', '--');
        plot(t_new, RPY_d(windows_plot,2)*r2g, 'Color', g, 'LineWidth', LineWidth, 'LineStyle', '--');
        set(gca,'Xtick',0:2:t_end)

        xlim(x_limits);
        grid on; 
        hold off;
        set(gca,'FontSize',FontSize_axes)        
        ylabel('$[\rm deg]$','Interpreter','LaTex','FontSize', FontSize_label);
        y_limits = ylimits(RPY_d(windows_plot,1:2)*r2g, perc_limits,alpha_max);
        ylim(y_limits);
        LegLocation = 'NorthEast'; %locationLegend(y_limits, eta(end,3)*r2g);
        h_leg = legend('$\phi$','$\vartheta$', '$\phi^*$', '$\vartheta^*$');
        set(h_leg,'orientation','horizontal','Location',LegLocation);
        set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');      
        set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
       
        i = i+1;
        
        ylabh1(ylabh1_index) = get(gca,'YLabel');
        ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
        ylabh1_index = ylabh1_index + 1;
        
        
        %% Roll & Pitch Dot
    axes(ha(i))
    set(gca,'FontSize',FontSize_axes)    
        plot(t_new, dRPY(windows_plot,1)*r2g, 'Color', r, 'LineWidth', LineWidth );
        hold on;
        plot(t_new, dRPY(windows_plot,2)*r2g, 'Color', g, 'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)
        plot(t_new, dRPY_d(windows_plot,1)*r2g, 'Color', r, 'LineWidth', LineWidth, 'LineStyle', '--');
        hold on;
        plot(t_new, dRPY_d(windows_plot,2)*r2g, 'Color', g, 'LineWidth', LineWidth, 'LineStyle', '--');
        set(gca,'Xtick',0:2:t_end)

        xlim(x_limits);
        grid on; 
        hold off;
        set(gca,'FontSize',FontSize_axes)        
        ylabel('$[\rm deg/s]$','Interpreter','LaTex','FontSize', FontSize_label);
        %y_avr = min(min(min(dRPY_d, dRPY))) + max(max(max(dRPY_d, dRPY)))/2;
        y_limits = ylimits([dRPY_d(windows_plot,1:2)*r2g], perc_limits,alpha_max);
        ylim(y_limits);
        LegLocation = 'NorthEast'; %locationLegend(y_limits, eta(end,3)*r2g);
        h_leg = legend('$\dot{\phi}$','$\dot{\vartheta}$', '$\dot{\phi^*}$','$\dot{\vartheta^*}$');
        set(h_leg,'orientation','horizontal','Location',LegLocation);
        set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');      
        set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
       
        i = i+1;
        %Ylabh1 %%%
        
        ylabh1(ylabh1_index) = get(gca,'YLabel');
        ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
        %ylabh2(ylabh1_index) = get(gca,'YLabel'); 
        %ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
        ylabh1_index = ylabh1_index + 1;
        %ylabh2_index = ylabh2_index + 1;
        
                %% Torque 
    axes(ha(i))
    set(gca,'FontSize',FontSize_axes)  
        h=plot(t_new, tauR_ctrl(windows_plot,1),'Color',b, 'LineWidth', LineWidth);
        hold on;
        plot(t_new, tauR_ctrl(windows_plot,2),'Color', r,'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)

        xlim(x_limits);
        grid on;        
        hold off;
        set(gca,'FontSize',FontSize_axes)
        ylabel('$[\rm Nm]$','Interpreter','LaTex','FontSize', FontSize_label); % ?????
        %y_limits = ylimits([tauR_ctrl(windows_plot_var,1),tauR_ctrl(windows_plot,1)],perc_limits,alpha_max);
        y_limits = ylimits([tauR_ctrl(windows_plot,1:2)], perc_limits,alpha_max);
        ylim(y_limits)
        %ylim([0 y_limits(2)])
        LegLocation = 'NorthEast';
        h_leg = legend('$\tau_x$','$\tau_y$');
        set(h_leg,'orientation','horizontal','Location',LegLocation);
        set(h_leg,'FontSize',FontSize,'Interpreter','LaTex'); 
        %xlabel('$[\rm s]$','Interpreter','LaTex','FontSize', FontSize_label);
        xlab = get(gca,'XLabel');
        xlabel_pos = get(xlab,'Position');
        text(x_limits(1)+0.5*(x_limits(2)-x_limits(1)),xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)), '$[\rm s]$', 'FontSize', FontSize);
        %text(preland_t0-0.5,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{L}$', 'FontSize', FontSize);
        %text(land_t0,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{G}$', 'FontSize', FontSize);
        
        %set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
        i = i+1;
        
        %%%Ylabh2
        ylabh2(ylabh1_index) = get(gca,'YLabel'); 
        ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
        ylabh1_index = ylabh1_index + 1;
        
               %% Motor speed 
    axes(ha(i))
    set(gca,'FontSize',FontSize_axes)  
        h=plot(t_new, motor_speed_actual(windows_plot,1),'Color',r, 'LineWidth', LineWidth);
        hold on;
        plot(t_new, motor_speed_actual(windows_plot,2),'Color', b,'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)
        plot(t_new, motor_speed_actual(windows_plot,3),'Color', y,'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)
        plot(t_new, motor_speed_actual(windows_plot,4),'Color', g,'LineWidth', LineWidth);
        set(gca,'Xtick',0:2:t_end)
%         plot(stime, motor_speed_des(:,1), '--','Color',r, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         plot(stime, motor_speed_des(:,2), '--','Color',b, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         plot(stime, motor_speed_des(:,3), '--','Color',y, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         plot(stime, motor_speed_des(:,4), '--','Color',g, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
        
        xlim(x_limits);
        grid on;        
        hold off;
        set(gca,'FontSize',FontSize_axes)
        ylabel('$[\rm Hz]$','Interpreter','LaTex','FontSize', FontSize_label);
        y_limits = ylimits([motor_speed_actual(windows_plot,1:4)],perc_limits,alpha_max);
        ylim(y_limits)
        LegLocation = 'NorthEast';
        h_leg = legend('$\omega_1$','$\omega_2$', '$\omega_3$', '$\omega_4$');
        set(h_leg,'orientation','horizontal','Location',LegLocation);
        set(h_leg,'FontSize',FontSize,'Interpreter','LaTex'); 
        %xlabel('$[\rm s]$','Interpreter','LaTex','FontSize', FontSize_label);
        xlab = get(gca,'XLabel');
        xlabel_pos = get(xlab,'Position');
        text(x_limits(1)+0.5*(x_limits(2)-x_limits(1)),xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)), '$[\rm s]$', 'FontSize', FontSize);
        %text(preland_t0-0.5,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{L}$', 'FontSize', FontSize);
        %text(land_t0,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{G}$', 'FontSize', FontSize);
        
        %set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
        %i = i+1;
        
        %%Ylabh2%%%
        ylabh2(ylabh1_index) = get(gca,'YLabel'); 
        ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
        %ylabh1_index = ylabh1_index + 1;
        
        

%% RPY Angles
% % eta=zeros(size(stime,1),3);
% % for k=time_window(1):time_window(end)
% %     R_=R(:,:,k);
% % %    Roll = atan2(R_(2,3),R_(3,3));
% % %    Pitch = -atan2(-R_(1,3),sqrt(R_(2,3)^2+R_(3,3)^2));
% % %    Yaw = atan2(R_(1,2),R_(1,1));
% % 	Roll=atan2(R_(2,1),R_(1,1));
% % 	Pitch=atan2(-R_(3,1),sqrt(R_(3,2)^2+R_(3,3)^2));
% % 	Yaw = atan2(R_(3,2),R_(3,3));
% %     eta(k,1)=Roll;
% %     eta(k,2)=Pitch;
% %     eta(k,3)=Yaw;
% % end
% eta=[-rpy_mocap(:,1),rpy_mocap(:,2),-rpy_mocap(:,3)];
% %prop_forces_opt__=[zeros(ceil(start_traj_time/dt),4);prop_forces_opt;zeros(size(stime_TH,1)-ceil(start_traj_time/dt)-size(prop_forces_opt,1),4)];

    %% Elevation
%     axes(ha(i));    
%         plot(t_new , y1_des(time_window_var,1)*r2g, '--', 'Color', b, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new , q(time_window,1)*r2g, 'Color', b, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         %line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         %line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on;        
%         hold off; 
%         set(gca,'FontSize',FontSize_axes) 
%         ylabel('$[\rm deg]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits([y1_des(windows_plot_var,1)*r2g, q(windows_plot,1)*r2g],perc_limits,0);
%         ylim(y_limits);
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, q(end,2)*r2g);        
%         h_leg = legend('$\varphi^d$','$\varphi$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         i = i+1;        
%         ylabh1(ylabh1_index) = get(gca,'YLabel');
%         ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
% %         ylabh2(1) = get(gca,'YLabel'); 
% %          ylabPos2(:,1) = get(ylabh2(1),'Position');
% %          
         %% Elevation velocity
%     axes(ha(i));    
%         plot(t_new, y1_des(time_window_THvar,2)*r2g, '--', 'Color', b, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, dq(time_window,1)*r2g, 'Color', b, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on;
%         hold off;
%         set(gca,'FontSize',FontSize_axes) 
%         ylabel('$[\rm deg/s]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits([y1_des(windows_plot_THvar,2)*r2g, dq(windows_plot,1)*r2g],perc_limits,alpha_max);
%         ylim(y_limits)
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, q(end,2)*r2g);        
%         h_leg = legend('$\dot{\varphi^d}$','$\dot{\varphi}$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         i = i+1;        
%         ylabh2(ylabh1_index) = get(gca,'YLabel'); 
%         ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
%         ylabh1_index = ylabh1_index + 1;  
% %         ylabh2(1) = get(gca,'YLabel'); 
% %          ylabPos2(:,1) = get(ylabh2(1),'Position');
    %% Azimuth
    
%     axes(ha(i));    
%         plot(t_new, y2_des(time_window_THvar,1)*r2g, '--', 'Color', r, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, q(time_window,2)*r2g, 'Color', r, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on;        
%         hold off;
%         set(gca,'FontSize',FontSize_axes) 
%         ylabel('$[\rm deg]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits([y2_des(windows_plot_THvar,1)*r2g, q(windows_plot,2)*r2g],perc_limits,0.3);
%         ylim(y_limits)
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, q(end,2)*r2g);        
%         h_leg = legend('$\delta^d$','$\delta$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         i = i+1;        
%         ylabh1(ylabh1_index) = get(gca,'YLabel');
%         ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
    %% Azimuth velocity
%     axes(ha(i));    
%         plot(t_new, y2_des(time_window_THvar,2)*r2g, '--', 'Color', r, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, dq(time_window,2)*r2g, 'Color', r, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on;        
%         hold off;
%         set(gca,'FontSize',FontSize_axes) 
%         ylabel('$[\rm deg/s]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits([y2_des(windows_plot_THvar,2)*r2g, dq(windows_plot,2)*r2g],perc_limits,alpha_max);
%         ylim(y_limits)
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, q(end,2)*r2g);        
%         h_leg = legend('$\dot{\delta^d}$','$\dot{\delta}$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         i = i+1;        
%         
%         ylabh2(ylabh1_index) = get(gca,'YLabel'); 
%         ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
%         ylabh1_index = ylabh1_index + 1;
        
    %% Theta 
%     axes(ha(i))
%         set(gca,'FontSize',FontSize_axes)  
%         plot(t_new, theta_des(time_window_THvar,1)*r2g, '--','Color',b, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, thetaA(time_window,1)*r2g,'Color', b,'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on;        
%         hold off;
%         set(gca,'FontSize',FontSize_axes)
%         ylabel('$[\rm deg]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits([theta_des(windows_plot_THvar,1)*r2g,thetaA(windows_plot,1)*r2g],perc_limits,alpha_max);
%         ylim(y_limits)
%         LegLocation = locationLegend(y_limits, thetaA(end,1));
%         h_leg = legend('$\vartheta_A^d$','$\vartheta_A$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');        
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         i = i+1;
%         ylabh1(ylabh1_index) = get(gca,'YLabel');
%         ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
%         
% %         ylabh1(2) = get(gca,'YLabel');
% %         ylabPos1(:,2) = get(ylabh1(2),'Position');
    %% Omega
%     axes(ha(i))
%     set(gca,'FontSize',FontSize_axes)    
%         plot(t_new, omegaR_filt(time_window,1), 'Color', r, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, omegaR_filt(time_window,2), 'Color', g, 'LineWidth', LineWidth);
%         plot(t_new, omegaR_filt(time_window,3), 'Color', b, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
%         dirty_o1=plot(t_new, omegaR(time_window,1), 'Color', r, 'LineWidth', LineWidth);
%         dirty_o1.Color(4)=omega_opacity;
%         dirty_o2=plot(t_new, omegaR(time_window,2), 'Color', g, 'LineWidth', LineWidth);
%         dirty_o2.Color(4)=omega_opacity;
%         dirty_o3=plot(t_new, omegaR(time_window,3), 'Color', b, 'LineWidth', LineWidth);
%         dirty_o3.Color(4)=omega_opacity;
%         %line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         %line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on; 
%         hold off;
%         set(gca,'FontSize',FontSize_axes)
%         ylabel('$[\rm deg/s]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits(omegaR_filt(windows_plot,:),perc_limits,alpha_max);
% %         y_limits = [-0.1 0.1];
%         ylim(y_limits);
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, u2(end));
%         h_leg = legend('$\omega_x$','$\omega_y$','$\omega_z$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');     
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
% %         xlabel('$[\rm s]$','Interpreter','LaTex','FontSize', FontSize_label);
%         i = i+1;
%         
%        ylabh2(ylabh1_index) = get(gca,'YLabel'); 
%         ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
%         ylabh1_index = ylabh1_index + 1;

        %% Yaw
%     axes(ha(i))
%     set(gca,'FontSize',FontSize_axes)    
%         plot(t_new, rpy_TH_des(time_window,3)*r2g,'--','Color', b, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, eta(time_window,3)*r2g, 'Color', b, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
% %         yaw_refl=refline([0 pi/4*r2g]);
% %         yaw_refl.LineStyle='--';
% %         yaw_refl.Color
% %         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
% %         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on; 
%         hold off;
%         set(gca,'FontSize',FontSize_axes)        
%         ylabel('$[\rm deg]$','Interpreter','LaTex','FontSize', FontSize_label);
%         %yaw_ref=pi/4*r2g*ones(windows_plot_THvar(end),1);
%         %y_limits = ylimits([eta(windows_plot,3)*r2g,yaw_ref(windows_plot_THvar)],perc_limits,alpha_max);
%         y_limits = ylimits([eta(windows_plot,3)*r2g,rpy_TH_des(windows_plot,3)*r2g],perc_limits,alpha_max);
%         ylim(y_limits);
%         LegLocation = 'NorthEast'; %locationLegend(y_limits, eta(end,3)*r2g);
%         h_leg = legend('$\psi$','$\psi^d$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');      
%         set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%        
%         i = i+1;
%         
%         ylabh2(ylabh1_index) = get(gca,'YLabel'); 
%         ylabPos2(:,ylabh1_index) = get(ylabh2(ylabh1_index),'Position'); 
%         ylabh1_index = ylabh1_index + 1;  
   
%     %% Thrust 
%     axes(ha(i))
%     set(gca,'FontSize',FontSize_axes)
%     set(gca,'XMinorTick','on');
% %         plot(t_new, prop_filt(time_window,1),'Color',r, 'LineWidth', LineWidth);
% %         hold on;
% %         plot(t_new, prop_filt(time_window,2),'Color',g, 'LineWidth', LineWidth);
% %         plot(t_new, prop_filt(time_window,3),'Color',b, 'LineWidth', LineWidth);
% %         plot(t_new, prop_filt(time_window,4),'Color',y, 'LineWidth', LineWidth);
%         plot(t_new, prop_forces(time_window,1),'Color',r, 'LineWidth', LineWidth);
%         hold on;
%         plot(t_new, prop_forces(time_window,2),'Color',g, 'LineWidth', LineWidth);
%         plot(t_new, prop_forces(time_window,3),'Color',b, 'LineWidth', LineWidth);
%         plot(t_new, prop_forces(time_window,4),'Color',y, 'LineWidth', LineWidth);
%         set(gca,'Xtick',0:2:t_end)
% %         dirty_f1=plot(t_new, prop_forces(time_window,1), 'Color', r, 'LineWidth', LineWidth);
% %         dirty_f1.Color(4)=thrust_opacity;
% %         dirty_f2=plot(t_new, prop_forces(time_window,2), 'Color', g, 'LineWidth', LineWidth);
% %         dirty_f2.Color(4)=thrust_opacity;
% %         dirty_f3=plot(t_new, prop_forces(time_window,3), 'Color', b, 'LineWidth', LineWidth);
% %         dirty_f3.Color(4)=thrust_opacity;
% %         dirty_f4=plot(t_new, prop_forces(time_window,4), 'Color', y, 'LineWidth', LineWidth);
% %         dirty_f4.Color(4)=thrust_opacity;
%         line(vline_preland(:,1),vline_preland(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         line(vline_land(:,1),vline_land(:,2),'Color', 'k', 'LineWidth', LineWidth);
%         xlim(x_limits);
%         grid on; 
%         set(gca,'FontSize',FontSize_axes)  
%         ylabel('$[\rm N]$','Interpreter','LaTex','FontSize', FontSize_label);
%         y_limits = ylimits(prop_filt(windows_plot,:),perc_limits,alpha_max);
%         %y_limits = ylimits(prop_forces_opt__(windows_plot_THvar,:),perc_limits,alpha_max);
%         ylim([0, y_limits(2)]);
%         LegLocation ='NorthEast';% locationLegend(y_limits, thrust_TH(end));
%         h_leg = legend('$f_1$','$f_2$','$f_3$','$f_4$');
%         set(h_leg,'orientation','horizontal','Location',LegLocation);
%         set(h_leg,'FontSize',FontSize,'Interpreter','LaTex');      
%         %set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
%         %xlabel('$[\rm s]$','Interpreter','LaTex','FontSize', FontSize_label);
%         xlab = get(gca,'XLabel');
%         xlabel_pos = get(xlab,'Position');
%         text(x_limits(1)+0.5*(x_limits(2)-x_limits(1)),xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)), '$[\rm s]$', 'FontSize', FontSize);
%         text(preland_t0-0.5,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{L}$', 'FontSize', FontSize);
%         text(land_t0,xlabel_pos(2) - 0.08*abs(y_limits(2) -y_limits(1)),'$t_{G}$', 'FontSize', FontSize);
%         %i = i+1;
%         ylabh1(ylabh1_index) = get(gca,'YLabel');
%         ylabPos1(:,ylabh1_index) = get(ylabh1(ylabh1_index),'Position');
% %         ylabh1(3) = get(gca,'YLabel');
% %         ylabPos1(:,3) = get(ylabh1(3),'Position');
%     

%% Correct Ylabel position
% % column 1
%  [minX1, minX1Index]  = min(ylabPos1(1,:));
%  for j=1:dim_raw
%      set(ylabh1(j),'Position',[minX1 ylabPos1(2,j) ylabPos1(3,j)]);
%  end
% % column 2
%   [minX2, minX2Index]  = min(ylabPos2(1,:));
%  for j=1:dim_raw
%      set(ylabh2(j),'Position',[minX2 ylabPos2(2,j) ylabPos2(3,j)]);
%  end

%% Save
if save_ck == 1
    set(gcf, 'PaperSize', [15 15]);
    set(gcf, 'PaperPositionMode', 'auto');
    print(fig,'-dpdf',strcat('images/','print_control'));
    system('pdfcrop images/print_control.pdf images/print_control.pdf');
end
