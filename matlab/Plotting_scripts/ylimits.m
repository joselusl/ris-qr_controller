function limits = ylimits( signals, delta, delta2)
% Compute the max and min for ylim
    Min_signals = min(min(signals)); 
    Max_signals = max(max(signals));
    range = abs(Max_signals - Min_signals);    
    Min = Min_signals - delta*range;    
    Max = Max_signals + delta*range + delta2*range;
    limits = [Min Max];
    if isequal(limits,[0 0])
        limits = [-0.1 0.1];
    end
end

