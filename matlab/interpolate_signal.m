function interpolated = interpolate_signal(signal_val, signal_time, time)

    % Values
    dim_signal=size(signal_val,2);
    num_samples_signal=size(signal_val,1);
    num_samples_total=size(time,1);

    % Initialize
    interpolated=zeros(num_samples_total, dim_signal);

    % Loop
    for(i=1:num_samples_total)
        % Find if exist
        elem_i=find(signal_time==time(i));
        if(~isempty(elem_i))
            interpolated(i,:)=signal_val(elem_i,:);
            continue;
        end
        
        % Search previous
        prev_elem=max(find(signal_time<time(i)));
        if(isempty(prev_elem))
           % Follow element is the First Element
           % TODO
           
           continue;
        end
        
        
        % Search following
        follow_elem=prev_elem+1;
        if(follow_elem>num_samples_signal)
            % Prev element is the Last Element
            % TODO
            
            continue;
        end
        
        % Interpolate
        interpolated(i,:)=(signal_val(follow_elem,:)-signal_val(prev_elem,:))/(signal_time(follow_elem)-signal_time(prev_elem))*(time(i)-signal_time(prev_elem))+signal_val(prev_elem,:);
        
        
    end

    


end