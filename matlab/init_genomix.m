%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% % Initialize GenoM stuff and connect the ports %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Don't forget to start the following console commands: 
% roscore, 
% optitrack-ros, 
% pom-ros,
% mikrokopter-ros, 
% genomixd

%% Path

% JL
openrobots_base_path='/home/jlsanche/devel/opt/openrobots/';
% Marco
% openrobots_base_path='/home/mtognon/openrobots/';

% General
addpath([openrobots_base_path,'lib/matlab/']);
addpath([openrobots_base_path,'lib/matlab/simulink']);
addpath([openrobots_base_path,'lib/matlab/simulink/genomix']);


clear;
clc;


%% Configs

flag_use_pom_mocap=true;

flag_use_pom_msf_localization=true;
flag_use_pom_msf_localization_mocap=false;

flag_start_optitrack=false; % if false, started externally


%% Genomix
disp('Initializing system');
% Localhost
client = genomix.client();
% Maemi
%client = genomix.client('140.93.16.106:8080');


%% Mikrokopter
mikro = client.load('mikrokopter');
pause(1);
result = mikro.connect({'/dev/ttyUSB0', ''}, 500000);
string = ['Connecting to mikrokopter: ',result.status];
disp(string);


%% Joystick
joy = client.load('joystick');
string = 'Connecting to joystick: done';
disp(string);


%% Optitrack
if(flag_start_optitrack)
    opti=client.load('optitrack');
    pause(1);
    result = opti.connect('marey','1510','239.192.168.30','1511');
    string = ['Connecting to MoCap: ',result.status];
    disp(string);
    pause(1);
end


%% POM
if(flag_use_pom_mocap)
    
    %% POM Object
    pom = client.load('pom','-i','pom_mocap');
    string = 'Connecting to POM: done';
    disp(string);
    pause(1);

    %% Connection POM to mk imu
    result = pom.connect_port('measure/imu', 'mikrokopter/imu');
    string = ['-Init. imu connection of POM: ',result.status];
    pom.add_measurement('imu');
    disp(string);

    %% Connection POM to optitrack
    result = pom.connect_port('measure/mocap', 'optitrack/bodies/QR_4');
    string = ['-Init. mocap connection of POM: ',result.status];
    pom.add_measurement('mocap');
    disp(string);
    
end


%% POM MSF Localization
if(flag_use_pom_msf_localization)
    
    %%  POM MSF Localization object
    pom_msf_localization = client.load('pom','-i','pom_msf_localization');
    string = 'Connecting to POM MSF Localization: done';
    disp(string);
    pause(1);

    %% Connection POM MSF Localization to mk imu
    result = pom_msf_localization.connect_port('measure/imu', 'mikrokopter/imu');
    string = ['-Init. imu connection of POM MSF Localization: ',result.status];
    pom_msf_localization.add_measurement('imu');
    disp(string);

    %% Connection POM MSF Localization to optitrack
    if(flag_use_pom_msf_localization_mocap)
        result = pom_msf_localization.connect_port('measure/mocap', 'optitrack/bodies/QR_4');
        string = ['-Init. mocap connection of POM MSF Localization: ',result.status];
        pom_msf_localization.add_measurement('mocap');
        disp(string);
    end

    %% Connection POM to msf_localization
    result = pom_msf_localization.connect_port('measure/estim_robot_state', '/fuseon/pom/robot_state_sensor');
    string = ['-Init. msf_localization connection of POM MSF Localization: ',result.status];
    pom_msf_localization.add_measurement('estim_robot_state');
    disp(string);

end





%% MK config
mikro.set_sensor_rate(1000,50,1); % IMU - motor - battery

%% Calibration
load('./Data/calibration_QR4.mat');
mikro.set_imu_calibration(calibration.result);



%% Run init
run('./init.m')



