%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Initialize GenoM stuff and connect the ports %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Don't forget to start the following console 

%% commands: roscore, optitrack-ros, pom-ros,

%% mikrokopter-ros, genomixd

% add genomix path

% JL
openrobots_base_path='/home/jlsanche/devel/opt/openrobots/';
% Marco
% openrobots_base_path='/home/mtognon/openrobots/';

% General
addpath([openrobots_base_path,'lib/matlab/']);
addpath([openrobots_base_path,'lib/matlab/simulink']);
addpath([openrobots_base_path,'lib/matlab/simulink/genomix']);


clear all;

disp('Initializing system');

% Genomix
client = genomix.client();

% Joystick
joy = client.load('joystick');
string = ['Connecting to joystick: done'];
disp(string);

