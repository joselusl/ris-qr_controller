 %% This function plots the values of simulation or real experiment

%% Code for the Callback Function in Model Properties
% 
% if quadrotorOn == 1
% mikro.enable_motor(4);
% pause(0.5);
% mikro.start();
% end
 
 close all;
%% Initial Parameters

r2d = 180/pi; %radians to degrees
switch_time = nonzeros(state_time);
    
%% Roll
figure('Name', 'Desired and Current Roll', 'NumberTitle', 'off');
grid on;
hold on;
plot(stime, RPY_d(:, 1)*r2d, '--');
plot(stime, RPY( :, 1)*r2d);
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(RPY*r2d)) max(max(RPY*r2d))], 'Color', 'b', 'LineStyle', '--');
end

%% Pitch
figure('Name', 'Desired and Current Pitch', 'NumberTitle', 'off');
grid on;
hold on;
plot(stime, RPY_d(:, 2)*r2d, '--');
plot(stime, RPY( :, 2)*r2d);
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(RPY*r2d)) max(max(RPY*r2d))], 'Color', 'b', 'LineStyle', '--');
end

%% Yaw
figure('Name', 'Desired and Current Yaw', 'NumberTitle', 'off');
grid on;
hold on;
plot(stime, RPY_d(:, 3)*r2d, '--');
plot(stime, RPY( :, 3)*r2d);
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(RPY*r2d)) max(max(RPY*r2d))], 'Color', 'b', 'LineStyle', '--');
end

%% Plot Commanded Torque
figure ('Name', 'Commanded Torque', 'NumberTitle', 'off');
grid on;
hold on;
plot(stime, tauR_ctrl);
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(tauR_ctrl)) max(max(tauR_ctrl))], 'Color', 'b', 'LineStyle', '--');
end

%% Plot motor speed saturation
figure('Name', 'Motor speed saturation', 'NumberTitle', 'off');
grid on;
hold on;
%plot(stime, motVel_max*ones(size(stime)), '--r'); %maximum thrust
%plot(stime, motVel_min*ones(size(stime)), '--r');  %minimum thrust
for i=1:4
     plot(stime, motor_speed_des(:, i), '--');
     %plot(stime, motor_speed_actual(:, i));
end
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [motVel_min motVel_max], 'Color', 'b', 'LineStyle', '--');
end
%% Plot motor speed saturation
figure('Name', 'Motor speed saturation', 'NumberTitle', 'off');
grid on;
hold on;
%plot(stime, motVel_max*ones(size(stime)), '--r'); %maximum thrust
%plot(stime, motVel_min*ones(size(stime)), '--r');  %minimum thrust
for i=1:4
     plot(stime, c_f*motor_speed_des(:, i).^2, '--');
     %plot(stime, motor_speed_actual(:, i));
end
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [motVel_min motVel_max], 'Color', 'b', 'LineStyle', '--');
end

%% OmegaR
figure('Name','omegaR')
plot(stime,omegarR_des(:,1),'--');
hold on;
plot(stime,omegaR(:,:));
grid on;

%% Position
figure('Name','position')
plot(stime,pR_des(:,1),'--r');
hold on;
plot(stime,pR_des(:,2),'--g');
plot(stime,pR_des(:,3),'--b');
plot(stime,pR(:,1),'r');
plot(stime,pR(:,2),'g');
plot(stime,pR(:,3),'b');
grid on;
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(min(pR_des, pR))) max(max(max(pR_des, pR)))], 'Color', 'b', 'LineStyle', '--');
end

%% Linear velocity
figure('Name','velocity')
plot(stime,dpR_des(:,1),'--r');
hold on;
plot(stime,dpR_des(:,2),'--g');
plot(stime,dpR_des(:,3),'--b');
plot(stime,dpR(:,1),'r');
plot(stime,dpR(:,2),'g');
plot(stime,dpR(:,3),'b');
grid on;
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(min(dpR_des, dpR))) max(max(max(dpR_des, dpR)))], 'Color', 'b', 'LineStyle', '--');
end

%% Linear acceleration
figure('Name','acceleration')
hold on;
plot(stime,ddpR_star(:,1),'--r');
plot(stime,ddpR_star(:,2),'--g');
plot(stime,ddpR_star(:,3),'--b');
plot(stime,ddpR(:,1),'r');
plot(stime,ddpR(:,2),'g');
plot(stime,ddpR(:,3),'b');
grid on;
for i=1: size(switch_time)
line([switch_time(i) switch_time(i)], [min(min(min(ddpR_des, ddpR))) max(max(max(ddpR_des, ddpR)))], 'Color', 'b', 'LineStyle', '--');
end

%% Frequency 
freq_avr = mean(frequency(10:end), 'omitnan')
plot(stime, frequency);
line([0 stime(end)], [freq_avr freq_avr], 'Color', 'r');
grid on;

%% Create a calibration structure
% calibration.status = 'done';
% calibration.result.imu_calibration.gscale =  {0.961169 0.0119993 0.00987107 -0.00591695 0.999648 0.0407578 -0.00391091 -0.0485107 0.997105};
% calibration.result.imu_calibration.gbias = {-0.101078 0.00943128 -0.110026};
% calibration.result.imu_calibration.gstddev = {0.00636214 0.00664608 0.00618665};
% calibration.result.imu_calibration.ascale = {0.977513 0.0296743 0.00913032 0.00062866 0.943527 0.0318986 0.0180974 -0.0910093 0.996701};
% calibration.result.imu_calibration.abias =  {-0.280667 -0.0493806 0.123845};
% calibration.result.imu_calibration.astddev = {0.0168737 0.0184687 0.046828};
% calibration.exception = [ ];
