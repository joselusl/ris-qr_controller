%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%    WAYPOINTS TRAJECTORY (QUBIC-SPLINE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file generate a piece-wise polynomial trajectory passing throught
% the desired way points at the desired time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%wayPoints = [0 0 0; 2 2 0; 0 4 0; 2 6 0];
wayPoints = [0 0 0; 2 0 0; 2 0 2; 0 0 2];
times = 5*ones(4,1) + [0; 5; 10; 15]/3;
initialVel = 0;
finalVel = 0;

traj = qSplineInterpolation(times, wayPoints);