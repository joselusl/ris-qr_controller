%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PARAMETER HIERARCHICAL GEOMETRIC CONTROLLER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sampling time
t_HC = 1/500;

% Poles
p_pR = 1.5*[-1 -2];           % desired poles tranlational dynamics  p_pR = 3.2*[-1 -2];  
p_omegaR = 8*[-1.5 -2.5];     % desired poles rotational dynamics     p_omegaR = 8.1*[-1.5 -2.5]


% ---------Pole placing translational dynamic------------------------------
A_pR = [0 1; 0 0];
B_pR = [0 1]';
K_pR = real(place(A_pR,B_pR,p_pR));
    
% Outer loop (position control)
hierGeomCtrl.Kp_pR = 9; %K_pR(1);     % Proportional gain
hierGeomCtrl.Kd_pR = 7; %K_pR(2);     % Derivative gain


% ---------Pole placing rotational dynamic---------------------------------
A_omegaR = [0 1; 0 0];
B_omegaR = [0 1]';
K_omegaR = real(place(A_omegaR,B_omegaR,p_omegaR));

% Inner loop (orientation control)
hierGeomCtrl.Kp_omegaR = K_omegaR(1);     % Proportional gain
hierGeomCtrl.Kd_omegaR = K_omegaR(2);     % Derivative gain