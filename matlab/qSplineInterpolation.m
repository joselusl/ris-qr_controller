%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     QUBIC SPLINE INTERPOLATION                                      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ trajectories ] = qSplineInterpolation(times, wayPoints, initialVelocity, finalVelocity)
% Given a n-dimensional wayPoints vector the function returns the 
% interpolating plynomium of 3th degree

% Set defaul initial and final Velocity if not decleared
switch nargin
    case 2
        initialVelocity = zeros(size(times,1),1);
        finalVelocity = zeros(size(times,1),1);
    case 3
        finalVelocity = zeros(size(times,1),1);
end

%spline_poly = {};   % initialize the vector of splines relative to each coordinates

% for every coordinate we compute the relative spline
for i=1:size(wayPoints,2)
    x_i = wayPoints(:,i);               % select the data way point for the i-th coordinate
    initialVel_i = initialVelocity(i);
    finalVel_i = finalVelocity(i);

    S =  spline(times, [initialVel_i; x_i; finalVel_i]);

    % Taking First Derivative 
    M = diag(3:-1:1,1); 
    S1 = S; 
    S1.coefs = S1.coefs*M;

    % Second derivative 
    S2 = S1; 
    S2.coefs = S2.coefs*M;
    
    trajectories(:,i) = [S; S1; S2];
end

end

